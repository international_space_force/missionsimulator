#ifndef CENTRALEXECUTIVEMODULE_CONCURRENTMAP_HPP
#define CENTRALEXECUTIVEMODULE_CONCURRENTMAP_HPP

#include <map>
#include <mutex>

template <typename K, typename V>
class ConcurrentMap {
private:
    std::map<K, V> data;
    std::mutex mapLock;

public:
    ConcurrentMap() {

    }

    ~ConcurrentMap() {

    }

    void InsertData(K key, V newData) {
        std::lock_guard<std::mutex>lck(mapLock);
        data.insert(std::pair<K, V>(key, newData));
    }

    int GetElementAt(K key) {
        std::lock_guard<std::mutex>lck(mapLock);
        return data[key];
    }

    void EraseDataAt(K key) {
        std::lock_guard<std::mutex>lck(mapLock);
        data.erase(key);
    }

    bool IsKeyPresent(K key) {
        std::lock_guard<std::mutex>lck(mapLock);
        return data.find(key) != data.end();
    }

    void UpdateKey(K key, V updatedData) {
        std::lock_guard<std::mutex>lck(mapLock);
        data[key] = updatedData;
    }
};

#endif //CENTRALEXECUTIVEMODULE_CONCURRENTMAP_HPP
