#ifndef CENTRALEXECUTIVEMODULE_MESSAGERETRY_HPP
#define CENTRALEXECUTIVEMODULE_MESSAGERETRY_HPP

#include "ConcurrentMap.hpp"

const int BEGINNING_RETRY_COUNT = 3;

void AddNewMsgToWaitForResponse(ConcurrentMap<uint16_t, int>& retryData, uint16_t key) {
    if (retryData.IsKeyPresent((key))) {
        // Not found
        retryData.InsertData(key, BEGINNING_RETRY_COUNT);
    } else {
        // Found
        int currentMsgWaitCount = retryData.GetElementAt(key);
        currentMsgWaitCount++;
        retryData.UpdateKey(key, currentMsgWaitCount);
    }
}

void DecrementMsgWaitResponse(ConcurrentMap<uint16_t, int>& retryData, uint16_t key) {
    if (retryData.IsKeyPresent((key))) {
        int currentMsgWaitCount = retryData.GetElementAt(key);
        currentMsgWaitCount--;
        if(currentMsgWaitCount < 0) {
            currentMsgWaitCount = 0;
        }
        retryData.UpdateKey(key, currentMsgWaitCount);
    }
}

void SetKeyToZero(ConcurrentMap<uint16_t, int>& retryData, uint16_t key) {
    if (retryData.IsKeyPresent(key)) {
        retryData.UpdateKey(key, 0);
    }
}

void RemoveMsgToBeWaitedOn(ConcurrentMap<uint16_t, int>& retryData, uint16_t key) {
    if (retryData.IsKeyPresent(key)) {
        retryData.EraseDataAt(key);
    }
}

#endif //CENTRALEXECUTIVEMODULE_MESSAGERETRY_HPP
