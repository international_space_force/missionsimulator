#ifndef CENTRALEXECUTIVEMODULE_MESSAGECREATIONSEND_HPP
#define CENTRALEXECUTIVEMODULE_MESSAGECREATIONSEND_HPP

#include <vector>
#include "../Headers/ConcurrentQueue.hpp"
#include "../MAVLink/common/mavlink.h"
#include "../Headers/ModuleDesignations.hpp"

const int MSG_DATA_SIZE_LIMIT = 3000;

std::vector<char> CharArrayToVector(char* inputData, int inputDataSize) {
    std::vector<char> result(inputData, inputData + inputDataSize);
    return result;
}

void VectorToCharArray(std::vector<char> inputData, char* outputData, int dataSizeLimit) {
    if(inputData.size() <= dataSizeLimit) {
        std::copy(inputData.begin(), inputData.end(), outputData);
    }
}

std::string MAVLinkEnumToString(MAVLinkMsgModule moduleID) {
    std::string result = "EMPTY";
    switch(moduleID) {
        case UltrasonicSensor:
            result = "Ultrasonic Sensor";
            break;
        case PathPlanning:
            result = "Path Planning";
            break;
        case MotorControl:
            result = "Motor Control";
            break;
        case SoldierRetrieval:
            result = "Soldier Retrieval";
            break;
        case RoverCommunication:
            result = "Rover Com";
            break;
        case CentralExecutive:
            result = "Central Executive";
            break;
        case IEDSensor:
            result = "IED Sensor";
            break;
        case CameraControl:
            result = "Camera Control";
            break;
        case GUI:
            result = "GUI";
            break;
        case CCSCommunication:
            result = "CCS Com";
            break;
        case MissionExecutive:
            result = "Mission Executive";
            break;
        default:
            result = "ERROR";
            break;
    }
    return result;
}

std::stringstream MAVLinkMsgToStringStream(mavlink_message_t msg, bool isIncomingData) {
    std::stringstream result;
    if(isIncomingData) {
        result << "Incoming ";
    } else {
        result << "Outgoing ";
    }
    result << "MAVLinkMsg - ";

    switch(msg.msgid) {
        case MAVLINK_MSG_ID_HEARTBEAT:
            result << "Heartbeat Msg" << std::endl;;
            break;

        case MAVLINK_MSG_ID_DISTANCE_SENSOR:
            result << "Ultrasonic Sensor Detection" << std::endl;
            break;

        case MAVLINK_MSG_ID_RAW_PRESSURE:
            result << "Environment Data" << std::endl;
            mavlink_raw_pressure_t rawPressureMsg;
            mavlink_msg_raw_pressure_decode(&msg, &rawPressureMsg);
            result << "Pressure: " << rawPressureMsg.press_abs << std::endl;
            result << "Altitude: " << rawPressureMsg.press_diff1 << std::endl;
            result << "Temperature: " << rawPressureMsg.temperature << std::endl;
            break;

        case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
            result << "Accelerometer Data" << std::endl;
            mavlink_local_position_ned_t accelerometerMsg;
            mavlink_msg_local_position_ned_decode(&msg, &accelerometerMsg);
            result << "X: " << accelerometerMsg.x << std::endl;
            result << "Y: " << accelerometerMsg.y << std::endl;
            result << "Z: " << accelerometerMsg.z << std::endl;
            break;

        case MAVLINK_MSG_ID_LANDING_TARGET:
            result << "Distance Remaining to Target" << std::endl;
            mavlink_landing_target_t distanceToTargetMessage;
            mavlink_msg_landing_target_decode(&msg, &distanceToTargetMessage);
            result << "Remaining Distance: " << distanceToTargetMessage.distance << std::endl;
            break;

        case MAVLINK_MSG_ID_MISSION_CURRENT:
            result << "IED Sensor Detection" << std::endl;
            break;

        case MAVLINK_MSG_ID_MISSION_ITEM_REACHED:
            result << "At Destination" << std::endl;
            break;

        default:
            break;
    }

    result << MAVLinkEnumToString((MAVLinkMsgModule)msg.sysid);
    result << " --> " << MAVLinkEnumToString((MAVLinkMsgModule)msg.compid);
    result << " "<< msg.checksum << '\n';

    if(msg.msgid == MAVLINK_MSG_ID_COMMAND_INT) {
        mavlink_command_int_t mavCmdInt;
        mavlink_msg_command_int_decode(&msg, &mavCmdInt);
        result << "CmdID: " << mavCmdInt.command << std::endl;
        switch (mavCmdInt.command) {
            case MAV_CMD_NAV_WAYPOINT:
                result << "New Waypoint Received" << std::endl;
                break;

            case MAV_CMD_DO_FLIGHTTERMINATION:
                if(mavCmdInt.param1 == 1) {
                    result << "Kill Switch: On" << std::endl;
                } else {
                    result << "Kill Switch: Off" << std::endl;
                }
                break;

            case MAV_CMD_DO_PAUSE_CONTINUE:
                if(mavCmdInt.param1 == 1) {
                    result << "Go Command" << std::endl;
                } else {
                    result << "Stop Command" << std::endl;
                }
                break;

            case MAV_CMD_DO_SET_MODE:
                if(mavCmdInt.param1 == MAV_MODE_MANUAL_ARMED) {
                    result << "ManMode: On" << std::endl;
                } else {
                    result << "ManMode: Off" << std::endl;
                }
                break;

            default:
                break;
        }
    }

    return result;
}

// 75, cmd 185
uint16_t SendKillSwitchStatus(ConcurrentQueue<std::vector<char>>& outgoingData, bool isKillSwitchOn, MAVLinkMsgModule source,
                              MAVLinkMsgModule dest, int dataSizeLimit, ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t killSwitchCmd;

    if(isKillSwitchOn) {
        mavlink_msg_command_int_pack(source, dest, &killSwitchCmd,
                                     source, dest, 0,
                                     MAV_CMD_DO_FLIGHTTERMINATION, 0, 0, 1,
                                     0, 0 , 0 , 0 , 0 , 0);
    } else {
        mavlink_msg_command_int_pack(source, dest, &killSwitchCmd,
                                     source, dest, 0,
                                     MAV_CMD_DO_FLIGHTTERMINATION, 0, 0, 0,
                                     0, 0 , 0 , 0 , 0 , 0);
    }

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &killSwitchCmd);
    outgoingData.push(CharArrayToVector((char*)&buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(killSwitchCmd, false));
    return killSwitchCmd.checksum;
}

// 75, cmd 193
uint16_t SendStopGoCommand(ConcurrentQueue<std::vector<char>>& outgoingData, bool isGo, MAVLinkMsgModule source,
                           MAVLinkMsgModule dest, int dataSizeLimit, ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t stopGoCmd;
    if(isGo) {
        mavlink_msg_command_int_pack(source, dest, &stopGoCmd,
                                     source, dest, 0,
                                     MAV_CMD_DO_PAUSE_CONTINUE, 0, 0, 1,
                                     0, 0 , 0 , 0 , 0 , 0);
    } else {
        mavlink_msg_command_int_pack(source, dest, &stopGoCmd,
                                     source, dest, 0,
                                     MAV_CMD_DO_PAUSE_CONTINUE, 0, 0, 0,
                                     0, 0 , 0 , 0 , 0 , 0);
    }

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &stopGoCmd);
    outgoingData.push(CharArrayToVector((char*)&buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(stopGoCmd, false));
    return stopGoCmd.checksum;
}

// 75, cmd 176
uint16_t SendManualModeStatus(ConcurrentQueue<std::vector<char>>& outgoingData, bool isManualModeOn, MAVLinkMsgModule source,
                              MAVLinkMsgModule dest, int dataSizeLimit, ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t manualModeStatusCmd;
    if(isManualModeOn) {
        mavlink_msg_command_int_pack(source, dest, &manualModeStatusCmd,
                                     source, dest, 0,
                                     MAV_CMD_DO_SET_MODE, 0, 0, MAV_MODE_MANUAL_ARMED,
                                     0, 0 , 0 , 0 , 0 , 0);
    } else {
        mavlink_msg_command_int_pack(source, dest, &manualModeStatusCmd,
                                     source, dest, 0,
                                     MAV_CMD_DO_SET_MODE, 0, 0, MAV_MODE_MANUAL_DISARMED,
                                     0, 0 , 0 , 0 , 0 , 0);
    }

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &manualModeStatusCmd);
    outgoingData.push(CharArrayToVector((char*)&buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(manualModeStatusCmd, false));
    return manualModeStatusCmd.checksum;
}

// 74, command 16
uint16_t SendWaypointLocation(ConcurrentQueue<std::vector<char>>& outgoingData, MAVLinkMsgModule source, MAVLinkMsgModule dest,
                              float latitude, float longitude, int dataSizeLimit, ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t waypointMessage;

    mavlink_msg_command_int_pack(source, dest, &waypointMessage,
                                 source, dest, 0,MAV_CMD_NAV_WAYPOINT, 0, 0,
                                 latitude, longitude, 0 , 0 , 0, 0 , 0);

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &waypointMessage);
    outgoingData.push(CharArrayToVector((char *) &buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(waypointMessage, false));
    return waypointMessage.checksum;
}

// 247
uint16_t SendCollisionMessage(ConcurrentQueue<std::vector<char>>& outgoingData, bool isFromUltrasonicSensor,
                              MAVLinkMsgModule source, MAVLinkMsgModule dest, int dataSizeLimit,
                              ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t collisionMessage;

    if(isFromUltrasonicSensor) {
        mavlink_msg_collision_pack(source, dest, &collisionMessage,
                                   MAV_COLLISION_SRC_ADSB, UltrasonicSensor,
                                   MAV_COLLISION_ACTION_HOVER, MAV_COLLISION_THREAT_LEVEL_LOW, 0.0,
                                   0.0, 0.0);
    } else {
        mavlink_msg_collision_pack(source, dest, &collisionMessage,
                                   MAV_COLLISION_SRC_ADSB, IEDSensor,
                                   MAV_COLLISION_ACTION_HOVER, MAV_COLLISION_THREAT_LEVEL_LOW, 0.0,
                                   0.0, 0.0);
    }

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &collisionMessage);
    outgoingData.push(CharArrayToVector((char *) &buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(collisionMessage, false));
    return collisionMessage.checksum;
}

uint16_t SendDistanceToLocationMsg(ConcurrentQueue<std::vector<char>>& outgoingData, MAVLinkMsgModule source,
                                   MAVLinkMsgModule dest, int dataSizeLimit, mavlink_landing_target_t distanceToTargetMessage,
                                   ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t distanceMsg;
    float zero = 0.0;

    mavlink_msg_landing_target_pack(source, dest, &distanceMsg, 0, 0, 0, 0,
                                    0, distanceToTargetMessage.distance,
                                    0, 0, 0, 0, 0, &zero, 0, 0);

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &distanceMsg);
    outgoingData.push(CharArrayToVector((char *) &buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(distanceMsg, false));
    return distanceMsg.checksum;
}

uint16_t SendAccelerometerDataMsg(ConcurrentQueue<std::vector<char>>& outgoingData, MAVLinkMsgModule source,
                                  MAVLinkMsgModule dest, int dataSizeLimit, uint32_t time_boot_ms, float x, float y,
                                  float z, ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t accelerometerData;

    mavlink_msg_local_position_ned_pack(source, dest, &accelerometerData, time_boot_ms,
                                        x, y, y, 0.0, 0.0, 0.0);

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &accelerometerData);
    outgoingData.push(CharArrayToVector((char *) &buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(accelerometerData, false));
    return accelerometerData.checksum;
}

uint16_t SendEnvironmentalDataMsg(ConcurrentQueue<std::vector<char>>& outgoingData, MAVLinkMsgModule source,
                                  MAVLinkMsgModule dest, int16_t press_abs, int16_t press_diff1, int16_t tempF,
                                  int dataSizeLimit, ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t environmentalData;

    mavlink_msg_raw_pressure_pack(source, dest, &environmentalData,0,
                                  press_abs, press_diff1, 0, tempF);

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &environmentalData);
    outgoingData.push(CharArrayToVector((char *) &buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(environmentalData, false));
    return environmentalData.checksum;
}

// 42
uint16_t SendIEDDetectionMessage(ConcurrentQueue<std::vector<char>>& outgoingData, MAVLinkMsgModule source,
                                 MAVLinkMsgModule dest, bool didDetectIED, int dataSizeLimit,
                                 ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t iedDetectionMsg;

    if(didDetectIED) {
        mavlink_msg_mission_current_pack(source, dest, &iedDetectionMsg, 1);
    } else {
        mavlink_msg_mission_current_pack(source, dest, &iedDetectionMsg, 0);
    }

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &iedDetectionMsg);
    outgoingData.push(CharArrayToVector((char *) &buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(iedDetectionMsg, false));
    return iedDetectionMsg.checksum;
}

uint16_t SendLocationReachedMessage(ConcurrentQueue<std::vector<char>>& outgoingData, MAVLinkMsgModule source,
                                    MAVLinkMsgModule dest, bool didReachLocation, int dataSizeLimit,
                                    ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t locationReachedMsg;

    if(didReachLocation) {
        mavlink_msg_mission_item_reached_pack(source, dest, &locationReachedMsg, 1);
    } else {
        mavlink_msg_mission_item_reached_pack(source, dest, &locationReachedMsg, 0);
    }

    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &locationReachedMsg);
    outgoingData.push(CharArrayToVector((char *) &buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(locationReachedMsg, false));
    return locationReachedMsg.checksum;
}

// This method will create a command ack to send back to a module that sent this
// module a message that needs a response
void SendAckBack(ConcurrentQueue<std::vector<char>>& outgoingData, MAVLinkMsgModule source, MAVLinkMsgModule dest,
                 uint16_t msgIDRespondingToChecksum, uint8_t msgID, uint8_t subID, int dataSizeLimit,
                 ConcurrentFileLogger& logger) {
    int sizeInBytes = 0;
    uint8_t buffer[MSG_DATA_SIZE_LIMIT];
    mavlink_message_t acknowledgement;
    mavlink_msg_command_ack_pack(source, dest, &acknowledgement, msgIDRespondingToChecksum, msgID,
                                 subID, 0, 0, 0);
    sizeInBytes = mavlink_msg_to_send_buffer(buffer, &acknowledgement);
    outgoingData.push(CharArrayToVector((char *) &buffer, sizeInBytes));
    logger.WriteToFile(MAVLinkMsgToStringStream(acknowledgement, false));
}

#endif //CENTRALEXECUTIVEMODULE_MESSAGECREATIONSEND_HPP
