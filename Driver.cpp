#include <cstdio>
#include <vector>
#include <iostream>

#include "Headers/UDPServer.hpp"
#include "Headers/ConcurrentQueue.hpp"
#include "Headers/UDPClient.hpp"
#include "Headers/ModuleDesignations.hpp"
#include "MAVLink/common/mavlink.h"
#include "Headers/ConcurrentFileLogger.hpp"
#include "Headers/MessageCreationSend.hpp"

#include "Windows.h"

const int DATA_LIMIT_SIZE = 3000;

const std::string LOOPBACK_ADDRESS = "127.0.0.1";

ConcurrentFileLogger logger("MissionSimModuleLog.txt");

[[noreturn]] void HandleIncomingData(UDPServer& serverConnection, ConcurrentQueue<std::vector<char>>& dataReceived) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;

        //printf("Waiting for local data: \n");
        serverConnection.GetDatagram(std::ref(receivedByteCount), receivedBuffer, DATA_LIMIT_SIZE);
        //printf("Received local data: %s\n", receivedBuffer);

        dataReceived.push(CharArrayToVector(receivedBuffer, receivedByteCount));
    }
}

[[noreturn]] void SendOutGoingData(UDPClient& clientModuleConnection, ConcurrentQueue<std::vector<char>>& queue) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int sendingByteCount = 0;
        std::vector<char> dataToBeSent;

        queue.pop(dataToBeSent);
        sendingByteCount = dataToBeSent.size();

        VectorToCharArray(dataToBeSent, receivedBuffer, DATA_LIMIT_SIZE);

        //printf("Outgoing Data\n");
        clientModuleConnection.SentDatagramToServerSuccessfully(receivedBuffer, sendingByteCount);
    }
}

int main() {
    // Data structures for handling local data
    ConcurrentQueue<std::vector<char>> incomingDataToParse;
    ConcurrentQueue<std::vector<char>> outgoingData;

    // UDP data connections
    UDPServer thisServer(MissionExecutiveModule, LOOPBACK_ADDRESS);
    UDPClient connectionToRoverCommunicationModule(GUIModule, LOOPBACK_ADDRESS);

    // Data connections
    std::thread handleIncomingData(HandleIncomingData, std::ref(thisServer), std::ref(incomingDataToParse));
    std::thread sendOutgoingData(SendOutGoingData, std::ref(connectionToRoverCommunicationModule),
                                 std::ref(outgoingData));

    printf("Mission Simulator Module Executing\n");

    int userSelection = -1;

    do{
        printf("Enter a number to send to the Central Executive Module\n");
        std::cout << "1: Send Kill Switch On" << std::endl;
        std::cout << "2: Send Kill Switch Off" << std::endl;
        std::cout << "3: Send Go Command" << std::endl;
        std::cout << "4: Send Stop Command" << std::endl;
        std::cout << "5: Send Manual Mode On" << std::endl;
        std::cout << "6: Send Manual Mode Off" << std::endl;
        std::cout << "7: Send New Waypoint" << std::endl;
        std::cout << "8: Send Motor Stop Command" << std::endl;
        std::cout << "9: Send Distance to Location" << std::endl;
        std::cout << "10: Send Accelerometer Data" << std::endl;
        std::cout << "11: Send Environmental Data" << std::endl;
        std::cout << "12: Send IED Detection On" << std::endl;
        std::cout << "13: Send IED Detection Off" << std::endl;
        std::cout << "14: Send Send Location Reached On" << std::endl;
        std::cout << "15: Send Send Location Reached Off" << std::endl;
        std::cout << "100: To Stop" << std::endl;

        std::cin >> userSelection;
        switch (userSelection) {
            case 1:
                SendKillSwitchStatus(outgoingData, true, MissionExecutive, GUI, DATA_LIMIT_SIZE, logger);
                break;

            case 2:
                SendKillSwitchStatus(outgoingData, false, MissionExecutive, GUI, DATA_LIMIT_SIZE, logger);
                break;

            case 3:
                SendStopGoCommand(outgoingData, true, MissionExecutive, GUI, DATA_LIMIT_SIZE, logger);
                break;

            case 4:
                SendStopGoCommand(outgoingData, false, MissionExecutive, GUI, DATA_LIMIT_SIZE, logger);
                break;

            case 5:
                SendManualModeStatus(outgoingData, true, MissionExecutive, GUI, DATA_LIMIT_SIZE, logger);
                break;

            case 6:
                SendManualModeStatus(outgoingData, false, MissionExecutive, GUI, DATA_LIMIT_SIZE, logger);
                break;

            case 7:
                SendWaypointLocation(outgoingData, MissionExecutive, GUI, 45.5, 48.5, DATA_LIMIT_SIZE, logger);
                break;

            case 8:
                SendCollisionMessage(outgoingData, true, MissionExecutive, GUI, DATA_LIMIT_SIZE, logger);
                break;

            case 9:
                mavlink_landing_target_t distanceToTargetMessage;
                distanceToTargetMessage.distance = 10;
                SendDistanceToLocationMsg(outgoingData, MissionExecutive, GUI, DATA_LIMIT_SIZE, distanceToTargetMessage, logger);
                break;

            case 10:
                SendAccelerometerDataMsg(outgoingData, MissionExecutive, GUI, DATA_LIMIT_SIZE, 10, 20, 30, 40, logger);
                break;

            case 11:
                SendEnvironmentalDataMsg(outgoingData, MissionExecutive, GUI, 10, 20, 30, DATA_LIMIT_SIZE, logger);
                break;

            case 12:
                SendIEDDetectionMessage(outgoingData, MissionExecutive, GUI, true, DATA_LIMIT_SIZE, logger);
                break;

            case 13:
                SendIEDDetectionMessage(outgoingData, MissionExecutive, GUI, false, DATA_LIMIT_SIZE, logger);
                break;

            case 14:
                SendLocationReachedMessage(outgoingData, MissionExecutive, GUI, true, DATA_LIMIT_SIZE, logger);
                break;

            case 15:
                SendAckBack(outgoingData, MissionExecutive, GUI, 0, 1, 2, DATA_LIMIT_SIZE, logger);
                break;

            default:
                printf("Invalid Input");
                break;
        }

    }while(userSelection != 100);

    printf("Control Input Stopped, Close Sim To Exit");
    handleIncomingData.join();
    sendOutgoingData.join();

    return 0;
}
