echo Building: Mission Simulator Module
g++ -pthread Driver.cpp -o missionsim
chmod +x missionsim
echo Done building, moving to rover executables...
cp ./missionsim ../roverexecutables/missionsim
echo Done moving, exiting...

